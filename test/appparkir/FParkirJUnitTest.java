/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package appparkir;

import appparkir.koneksi;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author depran
 */
public class FParkirJUnitTest {
    koneksi konek;
    
    @Before
    public void setUp() {
        konek = new koneksi();
        konek.koneksi();
    }  

    /*
        Test Method simpan jika nomor polisi kosong dan jenis belum dipilih
    */
    @Test
    public void tesSimpan1(){
        assertEquals("seharusnya sama","Nomor polisi belum diisi!", simpan("", "-Pilih Jenis Kendaraan-"));
    }
    
    /*
        Test Method simpan jika nomor polisi diisi dan jenis belum dipilih
    */
    @Test
    public void tesSimpan2(){
        assertEquals("seharusnya sama", "Jenis kendaraan belum dipilih", simpan("D 123 9283 E", "-Pilih Jenis Kendaraan-"));
    }
    
    /*
        Test Method simpan jika nomor polisi diisi dan jenis sudah dipilih
    */
    @Test
    public void tesSimpan3(){
        assertEquals("seharusnya sama", "Data berhasil disimpan", simpan("D 123 9283 E", "Motor"));
    }
    
    /*
     Method logika menyimpan data parkir masuk
    */ 
     public String simpan(String txtNoPol, String cbJenis) {
           // TODO add your handling code here:
        if (txtNoPol.isEmpty()) {
            return  "Nomor polisi belum diisi!";
        } else if (cbJenis.equals("-Pilih Jenis Kendaraan-")) {
            return "Jenis kendaraan belum dipilih";
        } else {
            String nopol, jenis, sql;
            nopol = txtNoPol;
            jenis = cbJenis.toString();

            sql = "INSERT INTO tb_parkir(no_pol,jenis,tgl_masuk,jam_masuk)"
                    + "VALUES('"+nopol+"','"+jenis+"',CURDATE(),CURTIME())";
            try{
                konek.st = konek.conn.createStatement();
                konek.st.execute(sql);
            }
            catch (SQLException e){
                return "Data gagal disimpan";
            }
          return  "Data berhasil disimpan";
        }
        //resetAll();
        //tampil_masuk();
        //tampil_keluar();
     }
}
